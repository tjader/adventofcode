#!/usr/bin/env python3

import sys

input = [i.strip() for i in open('input').readlines()]

trees = 0

x = 0
y = 0

while y < len(input):
	width = len(input[y])
	if input[y][x % width] == '#':
		trees += 1

	y += 1
	x += 3

print(trees)
