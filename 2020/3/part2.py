#!/usr/bin/env python3

import sys

input = [i.strip() for i in open('input').readlines()]

slopes = [
	(1, 1),
	(3, 1),
	(5, 1),
	(7, 1),
	(1, 2),
]
total_trees = 1

for dx, dy in slopes:
	x = 0
	y = 0
	trees = 0

	while y < len(input):
		width = len(input[y])
		if input[y][x % width] == '#':
			trees += 1

		y += dy
		x += dx

	total_trees *= trees

print(total_trees)
