#!/usr/bin/env python3

import itertools
import sys

input = list(int(i) for i in open('input').readlines())

for a, b, c in itertools.combinations(input, 3):
	if a + b + c == 2020:
		print(a * b * c)
		sys.exit(0)

print('not found')
sys.exit(1)
