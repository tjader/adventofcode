#!/usr/bin/env python3

import sys

input = sorted(int(i) for i in open('input').readlines())

small = 0
big = len(input) - 1

while small < big:
	if input[small] + input[big] == 2020:
		print(input[small] * input[big])
		sys.exit()

	if input[small] + input[big] > 2020:
		big -= 1
	else:
		small += 1

print('not found')
sys.exit(1)
