#!/usr/bin/env python3

import sys

input = open('input')

def validate(passport):
	required_fields = frozenset({'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'})

	if any(field not in passport for field in required_fields):
		return False


	if not 1920 <= int(passport['byr']) <= 2002:
		return False
	if not 2010 <= int(passport['iyr']) <= 2020:
		return False
	if not 2020 <= int(passport['eyr']) <= 2030:
		return False

	hgt_unit = passport['hgt'][-2:]
	if hgt_unit == 'cm':
		if not 150 <= int(passport['hgt'][:-2]) <= 193:
			return False
	elif hgt_unit == 'in':
		if not 59 <= int(passport['hgt'][:-2]) <= 76:
			return False
	else:
		return False

	if passport['hcl'][0] != '#' or any(char not in '0123456789abcdef' for char in passport['hcl'][1:]):
		return False

	if passport['ecl'] not in {'amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'}:
		return False

	if len(passport['pid']) != 9 or any(char not in '0123456789' for char in passport['pid']):
		return False
	
	return True

valid = 0

current = {}

for line in input.readlines():
	line = line.strip()
	
	if line == '':
		if validate(current):
			valid += 1

		current = {}
	else:
		for pair in line.split():
			key, value = pair.split(':')
			current[key] = value

if validate(current):
	valid += 1

print(valid)
