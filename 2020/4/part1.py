#!/usr/bin/env python3

import sys

input = open('input')

required_fields = frozenset({'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'})

valid = 0
current = set()

for line in input.readlines():
	line = line.strip()
	
	if line == '':
		if all(field in current for field in required_fields):
			valid += 1
		current = set()
	else:
		for pair in line.split():
			key, _ = pair.split(':')
			current.add(key)

if all(field in current for field in required_fields):
	valid += 1

print(valid)
