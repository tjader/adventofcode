#!/usr/bin/env python3

import sys

input = [i.strip() for i in open('input').readlines()]

valid = 0
invalid = 0

for line in input:
	policy, password = line.split(':')
	password = password.strip()

	pos, letter = policy.split()
	pos1, pos2 = (int(i) - 1 for i in pos.split('-'))

	if (password[pos1] + password[pos2]).count(letter) == 1:
		valid += 1
	else:
		invalid += 1

print(valid)
